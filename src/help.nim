proc getHelp*: string =
    return """Usage: dx11 COMMAND ...
 
Commands:
    dx11 new <name> <docker-file>     Create a new session and build its docker image
    dx11 run <name>                   Run x11docker on the specified session
    dx11 exec <name> [...]            Run ephemeral commands on an active session
    dx11 list                         Display the list of available sessions
    
    dx11 rebuild <name>               Rebuild the docker image of the session
    dx11 set <name> [...]             Update the configuration of the session
    dx11 delete <name>                Delete session's data and Docker image

    dx11 prune                        Clean unused Docker images
 
Options:
    -h --help                         Display the relevant help
 """

proc getNewHelp*: string =
    return """Usage: dx11 new <name> <docker-file> [opts]

Options:
    -e --encrypt       Encrypt the Home folder (needs ecryptfs-simple installed)
    -t --tor           Use Tor networking
    -h --help          Show this help message

Creates a DX11 session with the given name and builds the underlying Docker image that will be used.
If a session with this name already exists, it will prompt to update its current configuration while keeping the current user data.

NOTE: The path of the Dockerfile will be used as the base for the Docker context. Any files contained on this directory will be sent to the Docker server.

You should place your Dockerfile on a folder with just the files you want to add into the image.
"""

proc getRunHelp*: string =
    return """Usage: dx11 run <session-name>

Runs the DX11 session using the current configuration
"""

proc getExecHelp*: string =
    return """Usage: dx11 exec <name> [...]

With no additional parameters, executes "sh" on the target session. The session must be already active.
With additional parameters, executes them as a command on the guest machine. 
"""

proc getRebuildHelp*: string =
    return """Usage: dx11 rebuild <name>

Builds the Docker image from scratch and tags it for use with the given session.

The user's home data and settings are always preserved.
"""

proc getSetHelp*: string =
    return """Usage: dx11 set <name> [param1=value] ...

Updates the config of the given session.

Parameters:
    -t --tor           Use Tor networking

Options:
    -h --help          Show this help message
"""

proc getDeleteHelp*: string =
    return """Usage: dx11 delete <session-name>

Deletes the Docker image, the settings and the user data for the given session.
"""
