.DEFAULT_GOAL := help
PROJECTNAME=$(shell basename "$(PWD)")
SOURCES=$(sort $(wildcard ./src/*.nim))

.PHONY: help
help: makefile
	@echo " Available actions on "$(PROJECTNAME)":"
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
	@echo

## init: Install missing dependencies
.PHONY: init
init:
	@nimble install -y

## :

## all: Compile the binary executable
all: build/dx11

build/dx11: build $(SOURCES)
	@nimble build -y

## clean: Clean the build folder
.PHONY: clean
clean: 
	@echo Cleaning ./build
	@rm -Rf ./build/*

# the build folder
build: 
	mkdir -p $@
